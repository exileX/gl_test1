import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// import registerServiceWorker from './registerServiceWorker';
import { ThemeProvider, injectGlobal } from 'styled-components';
import buildGlobalStyles from './config/buildGlobalStyles';
import theme from './config/theme';
import configureFonts from './config/configureFonts';

injectGlobal([buildGlobalStyles(theme)]);
configureFonts(theme);

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <App />
    </ThemeProvider>
    ,

    document.getElementById('root'));
// registerServiceWorker();
