import {all, fork} from 'redux-saga/effects'
import {watchLoadGrid} from './grid'

export default function* rootSaga() {
    yield all([fork(watchLoadGrid)])
}
