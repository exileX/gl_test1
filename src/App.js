import React, {Component, Fragment} from 'react';
import {Provider} from 'react-redux'
import {BrowserRouter, Route} from 'react-router-dom'
import {store} from './store'
import Grid from './components/grid'

class App extends Component {
    render() {
        return (
            <Fragment>
                <Provider store={store}>
                    <BrowserRouter>
                        <Fragment>

                            <Grid />

                        </Fragment>
                    </BrowserRouter>
                </Provider>
            </Fragment>
        );
    }
}

export default App;
