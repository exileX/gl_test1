function buildGlobalStyles(theme) {
    return `
    html {
      height: 100%;
    }
    body {
      background-color: ${theme.colors.background};
      color: ${theme.colors.text};
      font-family: ${theme.fonts.primaryFallback};
      font-size: ${theme.dimensions.baseFontSize}px;
      font-weight: 400;
      height: 100%;
      margin: 0;
      button,
      input,
      select,
      textarea {
        font-family: ${theme.fonts.primaryFallback};
      }
      &.fonts-loaded {
        font-family: ${theme.fonts.primary};
        button,
        input,
        select,
        textarea {
          font-family: ${theme.fonts.primary};
        }
      }
      table {
        margin: 30px;
        td {
           color: ${theme.colors.blueGrey};
           font-size: ${theme.dimensions.tableFontSize}px;
           padding: 0 15px;
        }
      }
      
      form {
        margin: 30px;
        input, select, button {
          font-size: ${theme.dimensions.formFontSize}px;
          margin: 5px;
        }
      }
    }
    
  `;
}

export default buildGlobalStyles;