const REQUEST = 'REQUEST'
const SUCCESS = 'SUCCESS'
const FAILURE = 'FAILURE'

export const LOAD_GRID = 'LOAD_GRID'

export const GRID = createRequestTypes('GRID')

function createRequestTypes(base) {
    return [REQUEST, SUCCESS, FAILURE].reduce((acc, type) => {
        acc[type] = `${base}_${type}`
        return acc
    }, {})
}

function action(type, payload = {}) {
    return {type, ...payload}
}

export const grid = {
    request: () => action(GRID[REQUEST], {}),
    success: (response) => action(GRID[SUCCESS], {response}),
    failure: (error) => action(GRID[FAILURE], {error}),
}

export const loadGridItems = () => ({type: LOAD_GRID})
