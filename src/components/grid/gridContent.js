import React, {Component, Fragment} from 'react'
import GridItem from './gridItem'
import PropTypes from "prop-types";


export default class GridContent extends Component {
    static propTypes = {
        items: PropTypes.array
    }

    static defaultProps = {
        item: []
    }

    constructor(props) {
        super(props)
    }

    render() {
        const {items} = this.props

        return (
            <Fragment>
                <table>
                    <thead>
                    <tr>
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Age</th>
                    </tr>
                    </thead>
                    <tfoot></tfoot>
                    <tbody>
                    {
                        items.map((item) => <GridItem item={item} key={item.email}/>)
                    }
                    </tbody>
                </table>
            </Fragment>
        )
    }
}