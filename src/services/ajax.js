import axios from 'axios'

export default function ajax(endpoint, body = {}) {
    return axios(endpoint, body).then((json) => json.data.results)
}