import {call, put, take, fork, select} from 'redux-saga/effects'
import * as actions from '../actions/grid'
import {api} from '../services'
import React from "react";

function* fetchEntity(entity, apiFn) {
    yield put(entity.request())

    try {
        const response = yield call(apiFn)

        yield put(entity.success(response))
    } catch (error) {
        yield put(entity.failure(error))
    }
}

export const fetchGrid = fetchEntity.bind(null, actions.grid, api.fetchGrid)

function* loadGrid() {
    const menu = yield select((state) => state.grid)

    if (!Object.keys(menu).length) {
        yield call(fetchGrid)
    }
}

export function* watchLoadGrid() {
    while (true) {
        yield take(actions.LOAD_GRID)
        yield fork(loadGrid)
    }
}
