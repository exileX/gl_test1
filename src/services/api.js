import ajax from './ajax'

export const fetchGrid = () => callApi('https://randomuser.me/api/?results=30')

function callApi(endpoint, body = {}) {
    return ajax({
        url: endpoint,
        body: body,
    })
}
