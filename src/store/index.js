import {createStore, applyMiddleware} from 'redux'
import {createLogger} from 'redux-logger'
import createSagaMiddleware from 'redux-saga'

import rootSaga from '../sagas'
import {reducers} from '../reducers'

const middlewares = []
const sagaMiddleware = createSagaMiddleware()
middlewares.push(sagaMiddleware)

// if (process.env.NODE_ENV === `development`) {
    middlewares.push(createLogger())
// }

export const store = createStore(reducers, applyMiddleware(...middlewares))

sagaMiddleware.run(rootSaga)

//const action = type => store.dispatch({type})
