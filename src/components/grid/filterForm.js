import React, {Component} from 'react'
import { withRouter } from "react-router-dom";
import PropTypes from 'prop-types'

class FilterForm extends Component {
    static propTypes = {
        gender: PropTypes.string,
        email: PropTypes.string,
        onSubmit: PropTypes.func,
        onReset: PropTypes.func,
    }

    static defaultProps = {
        gender: '',
        email: '',
        onSubmit: () => {},
        onReset: () => {},
    }

    constructor(props) {
        super(props)
    }
    
    render() {
        const {gender, email} = this.props

        return (
            <form action="#" onSubmit={this.handleSubmit} key={`${gender}_${email}`}>
                <select name="gender" id="gender" defaultValue={gender}>
                    <option value="">No matter</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
                <input name="email" type="email" defaultValue={email} />
                <input name="submit" type="submit" value="Apply" />
                <button onClick={this.handleReset}>Reset filter</button>
            </form>
        )
    }

    handleSubmit = (e) => {
        e.preventDefault()

        let data = new FormData(e.target)
        this.props.onSubmit(data)
    }

    handleReset = (e) => {
        e.preventDefault()

        this.props.onReset()
    }
}

export default withRouter(FilterForm)