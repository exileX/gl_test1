import {combineReducers} from 'redux'
import gridReducer from './grid'

export const reducers = combineReducers({
    grid: gridReducer,
})
