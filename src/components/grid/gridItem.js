import React, {Component, Fragment} from 'react'
import PropTypes from "prop-types";

export default class GridItem extends Component {
    static propTypes = {
        item: PropTypes.shape({
            gender: PropTypes.string,
            email: PropTypes.string,
            phone: PropTypes.string,
            dob: PropTypes.shape({
                age: PropTypes.number,
                date: PropTypes.string
            }),

        }),
    }

    static defaultProps = {
        item: {
            gender: '',
            email: '',
            phone: '',
            dob: {
                age: 0,
                date: ''
            },
        },
    }

    constructor(props) {
        super(props)
    }

    render() {
        const {item} = this.props

        return (
            <Fragment>
                <tr>
                    <td>{item.gender}</td>
                    <td>{item.email}</td>
                    <td>{item.phone}</td>
                    <td>{item.dob.age}</td>
                </tr>
            </Fragment>
        )
    }
}