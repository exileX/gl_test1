export default function gridItems(state = {}, action) {
    const {type, response} = action

    switch (type) {
        case 'GRID_SUCCESS':
            return {
                ...response,
            }
        default:
            return state
    }
}
