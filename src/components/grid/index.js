import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import queryString from 'query-string';

import { withRouter } from 'react-router-dom';

import FilterForm from './filterForm'
import GridContent from './gridContent'
import {loadGridItems} from '../../actions/grid'

class Grid extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filterData: queryString.parse(props.location.search)
        }
    }

    componentDidMount() {
        this.props.loadGridItems()
        this.props.history.listen(this.locationChange());
    }

    locationChange = (location, action) => (location) => this.setState({filterData: queryString.parse(location.search)})

    filterApply = (data) => {
        this.props.history.push(`/users?gender=${data.get('gender')}&email=${data.get('email')}`);
        this.setState({
            filterData: {
                gender: data.get('gender'),
                email: data.get('email')
            }
        })
    }

    filterReset = () => {
        this.props.history.push(`/users`);
        this.setState({
            filterData: {
                gender: '',
                email: ''
            }
        })
    }

    prepareGridData = (data, filter) => Object.values(data)
        .map((item) => item)
        .filter((item) => (filter.gender ? item.gender === filter.gender : true) && (filter.email ? item.email === filter.email.trim() : true))


    render() {
        const {gender, email} = this.state.filterData

        return (
            <Fragment>
                <FilterForm onSubmit={this.filterApply} onReset={this.filterReset} gender={gender} email={email} />
                <GridContent items={this.prepareGridData(this.props.grid, {gender, email})}/>
            </Fragment>
        )
    }
}


export default connect(
    (state) => ({
        grid: state.grid,
    }),
    {
        loadGridItems,
    }
)(withRouter(Grid))
